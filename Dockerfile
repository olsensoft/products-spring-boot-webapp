FROM openjdk:11.0

ADD target/products-spring-boot-webapp-0.0.1.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/app.jar"]
